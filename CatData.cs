using System;

namespace DeploymentExample
{
    public static class CatData
    {
        public static Cat[] Cats =
        {
            new Cat(Guid.Parse("24c7da2c-6fac-4de2-8a10-f210e23a3d42"), "Maggie", new Uri("/assets/cats/3-cat-01.jpg", UriKind.Relative)),
            new Cat(Guid.Parse("e07e7a02-3665-48ed-a2dc-b58af21aa1f1"), "Sophie", new Uri("/assets/cats/3-cat-02.jpg", UriKind.Relative)),
            new Cat(Guid.Parse("cab2d5ee-4f19-4059-a861-48102683408e"), "Punkin", new Uri("/assets/cats/3-cat-03.jpg", UriKind.Relative)),
            new Cat(Guid.Parse("1d910279-55c5-47e9-adc8-c4b2c3664bec"), "Huckleberry", new Uri("/assets/cats/3-cat-04.jpg", UriKind.Relative)),
            new Cat(Guid.Parse("7a905f22-dc4e-49f9-b00e-dfcf3321443d"), "Spackle", new Uri("/assets/cats/3-cat-05.jpg", UriKind.Relative)),
        };
    }
}
FROM mcr.microsoft.com/dotnet/sdk:5.0

WORKDIR /app/src
COPY . .
RUN dotnet publish -c Release -o /app/out
WORKDIR /app/out

ENTRYPOINT ["dotnet", "DeploymentExample.dll"]
